package fr.mobarzik.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.mobarzik.controlers.ChooseEvent;
import fr.mobarzik.controlers.StartEvent;

public class App extends JFrame{

	private static JLabel FileName;
	private static File choosenFile;
	
	
	public static void main(String[] args) {
		
		App app = new App();
		FileName = new JLabel();
		app.setTitle("Pseudo-Code Translator");

		JPanel north_pane = new JPanel();
		JButton B_Choose = new JButton("Choisir un fichier");
		B_Choose.addMouseListener(new ChooseEvent(app));
		north_pane.setLayout(new BorderLayout());
		
		north_pane.add(B_Choose,BorderLayout.WEST);
		north_pane.add(FileName,BorderLayout.CENTER);
		
		app.setLayout(new BorderLayout());
		
		
		JButton start = new JButton("Commencer");
		
		start.addMouseListener(new StartEvent(app));
		
		app.add(north_pane, BorderLayout.NORTH);
		app.add(start,BorderLayout.CENTER);
		app.setVisible(true);
		app.setSize(200, 200);
		
	}
	
	public void setFileName(String name)
	{
		FileName.setText(name);
	}
	
	public void setFile(File f)
	{
		choosenFile = f;
	}
	
	public File getFile()
	{
		return choosenFile;
	}

}
