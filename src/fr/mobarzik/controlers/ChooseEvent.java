package fr.mobarzik.controlers;

import java.awt.event.MouseEvent;

import javax.swing.JFileChooser;
import javax.swing.event.MouseInputListener;

import fr.mobarzik.views.App;

public class ChooseEvent implements MouseInputListener {

	private App app;
	
	
	
	public ChooseEvent(App app) {
		this.app = app;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDragEnabled(true);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int ret = fileChooser.showOpenDialog(this.app);
		
		if(ret == JFileChooser.APPROVE_OPTION)
		{
			
			app.setFileName(fileChooser.getSelectedFile().getName());
			app.setFile(fileChooser.getSelectedFile());
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
