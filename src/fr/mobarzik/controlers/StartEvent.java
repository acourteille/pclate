package fr.mobarzik.controlers;

import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.event.MouseInputListener;

import fr.mobarzik.models.Translator;
import fr.mobarzik.views.App;

public class StartEvent implements MouseInputListener {

	private App app;
	
	
	
	public StartEvent(App app) {
		this.app = app;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		
		if(app.getFile() == null)
		{
			JOptionPane.showMessageDialog(app, "Erreur : Vous n'avez pas selectionné de fichier");
		}else {
			
			Translator t = new Translator(app.getFile(), app);
			try {
				t.translate();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
