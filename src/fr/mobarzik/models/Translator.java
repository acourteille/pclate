package fr.mobarzik.models;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.Collator;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import fr.mobarzik.views.App;

public class Translator {

	private File file;
	private App app;
	
	public Translator(File file, App app) {

		this.file = file;
		this.app = app;
	}
	
	
	public void translate() throws IOException
	{
		try {
			Scanner sc = new Scanner(this.file);
			
			String f = "";
			
			while(sc.hasNext())
			{
				f += sc.nextLine();
				f += System.lineSeparator();
			}
			
			
			f = this.DeleteComments(f);
			HashMap<String, String> variable = this.getVariables(f);
			
			if(variable != null)
			{
				System.out.println(variable.toString());
				String nativeDir = this.file.getPath().substring(0, this.file.getPath().lastIndexOf(File.separator));
				System.out.println(nativeDir);
				File output = new File(nativeDir + "/output.c");
				
				System.out.println(output);
				output.createNewFile();
				PrintWriter writer = new PrintWriter(output);
				writer.print("");
				writer.append("#include <stdio.h> \n" + 
						"\n" + 
						"int main() { \n");
				
				writer.append("\n/*"
						+ "\n Variables générées par PCLate depuis " + this.file.getAbsolutePath() 
						+ "\n*/\n");
			       final Collator instance = Collator.getInstance();
			       
			       instance.setStrength(Collator.NO_DECOMPOSITION);
				for (Map.Entry<String, String> entry : variable.entrySet()) {
			       System.out.println("clé " + entry.getKey()+" valeur " + entry.getValue());
			       if(instance.compare(entry.getValue(), "entier") == 0);
			       	writer.append("int" + entry.getKey() + ";\n");
			       	
			       if(instance.compare(entry.getValue(), "caractere") == 0)
			       {
			    	   writer.append("char" + entry.getKey() + ";\n");
					}
			       
			       if(instance.compare(entry.getValue(), "reel") == 0)
			       {
			    	   writer.append("float" + entry.getKey() + ";\n");
					}
					
			    }
				writer.close();
			
				
			}else {
				return;
			}
			
			
			
			
			
		} catch (FileNotFoundException e) {
			
			JOptionPane.showMessageDialog(app,e.getMessage());
		}
	}
	
	
	private String DeleteComments(String f)
	{
		return f.replaceAll("(\\{.*\\})", "");
	}
	
	private HashMap<String, String> getVariables(String f)
	{
		HashMap<String, String> variable = new HashMap<>();
		
		 Pattern pattern = Pattern.compile("([A-z].* : [A-z].*)");
		 
		 Matcher m = pattern.matcher(f);
		 boolean probleme = false;
		 while (m.find() && !probleme) {
			    String match = m.group(0);
			    
			    List<String> array =  Arrays.asList(match.split(":"));
			    
			    if(!array.isEmpty()) {
			    	if(variable.containsKey(array.get(1))) {
			    		JOptionPane.showMessageDialog(app,"Plusieurs variable ont le meme nom");
			    		probleme = true;
			    	}else{
			    		variable.put(array.get(1), array.get(0));
			    	}
			    	
			    
			    }
			    
			}
 		if(!probleme)

 			return variable;	
 		else
 			return null;
		
		
	}
	
	
}
